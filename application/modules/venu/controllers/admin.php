<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		//$this->load->module('admin_login/admin_login');
		//$this->admin_login->check_session_and_permission('venu'); //module name is groups here	
	}
	
	function index()
	{	
           
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
                $this->load->view("admin/table",$data);
                
                //above process can be done on this way also
                //$data['view_file']="admin/table";
               // echo Modules::run('template/admin_template',$data);
	}
	
//	function get_data_from_post()
//	{
//		$data['name'] = $this->input->post('name', TRUE);		
//		
//			$update_id = $this->input->post('update_id', TRUE);
//		return $data;
//	}
//	
        
        function get_data_from_post()
	{
		$data['title'] = $this->input->post('title', TRUE);
		$data['slug'] = strtolower(url_title($data['title']));
                $data['status'] = $this->input->post('status', TRUE);
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['title'] = $row->title;
                        $data['slug'] = $row->slug;
                        $data['status'] = $row->status;	

		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
//	function get_comments_from_db($update_id)
//	{
//		$query = $this->get_where($update_id);
//		foreach($venu->result() as $row)
//		{			
//			$data['comments'] = $row->comments;				
//		}
//			return $data;
//	}
	
	function create()
	{
            
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
	
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
		
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
                        
                        $data['block_list']=$this->block_list();
				
			$data['view_file'] = "admin/form";
			 $this->load->view("admin/form",$data);	
	}
	
         function submit()
        {          
            $data = $this->get_data_from_post();
            
            $update_id = $this->input->post('update_id', TRUE);
            if(is_numeric($update_id)){				
                    $this->_update($update_id, $data);
            } else {
            
                    $this->_insert($data);
                   
            }
            redirect('admin/venu');
        }
		
function block_list() {
        $this->load->model('block/mdl_block');
        $query = $this->mdl_block->get_groups_dropdown3();
        if (empty($query)) {
            return NULL;
        }
        return $query;
    }
	function get($order_by){
	$this->load->model('mdl_venu');
	$query = $this->mdl_venu->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_venu');
	$query = $this->mdl_venu->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_venu');
	$this->mdl_venu->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_venu');
	$this->mdl_venu->_update($id, $data);
	}
        
        function delete()
	{	$this->load->model('mdl_venu');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/venu');
			}
		else
		{
			$this->mdl_venu->_delete($delete_id);
			redirect('admin/venu');
		}			
			
	}
}