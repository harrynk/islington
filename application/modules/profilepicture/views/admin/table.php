<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Reports-RTE-Attendance de' Islington</title>
	<link rel="shortcut icon" href="<?php echo base_url();?>nimesh/img/titleLogo.png">
	<link href="<?php echo base_url();?>nimesh/css/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>nimesh/css/rte profile/rte_view_attendance_bootstrap.css" rel="stylesheet">
        <!--script for datatable-->
<script type="text/javascript" src="<?php echo base_url();?>design/admin/js/jquery.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>design/dataTable/jquery.dataTables.min.css" >
<script type="text/javascript" src="<?php echo base_url();?>design/dataTable/jquery.dataTables.min.js"></script>
<!--end script for datatable-->
 
         <script>
    $(document).ready(function(){
        $('#dataTables').dataTable();
    });
    </script>
</head>
<body id="mainBody">
	<div class="container-fluid">
		<div class="row">
			<div id="sideSection" class="col-sm-12 col-sx-12 col-md-3 col-lg-3">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div id="profilePic" class="col-md-8 col-md-offset-2">
								<figure id="tm_pp" class="thumbnail logo-thumbnail">
									<img src="<?php echo base_url();?>nimesh/img/profilePicture.jpg"/> 
								</figure>
							</div>
						</div>
						<div class="row text-center blue-text">
							<span class="glyphicon glyphicon-edit" aria-hidden="true"> Edit Picture</span>
						</div>
						<div class="row text-center blue-text">
							<button class="btn btn-default">Chose file</button>
						</div>
						<ul class="list-group">
							<li class="navSidebar list-group-item">
								<a href="<?php echo base_url();?>admin/dashboard">My Dashboard</a>
							</li>
							<li class="navSidebar list-group-item">
								<a style="color:#E70F12;">View Attendance</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Void Class</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">View Reports</a>
							</li>
							<li class="navSidebar list-group-item">
                                                            <a href="<?php echo base_url();?>teacher/admin/create">Add Teachers</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Manage Schedule</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Manage Groups</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Import Students</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Edit My Profile</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="<?php echo base_url();?>admin">Log Out</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-lg-9">
				<div id="header" class="row">
					<div id="headings" class="col-md-9 text-center">
						<h1 class="blue-text">ATTENDANCE DE' ISLINGTON</h1>
						<h3 class="red-text">Reports - RTE</h3>
					</div>
					<div id="logoContainer" class="col-md-3">
						<figure id="appLogo" class="thumbnail logo-thumbnail">
							<img src="<?php echo base_url();?>nimesh/img/logo.png"/>
						</figure>
					</div>
				</div>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol>
				<div class="row">
					<div id="middleSection">
						<div id="rteHome" class="col-md-8"> 
							
		
									
									
								<div class="panel panel-default">
                        <div class="panel-heading">
                            <input type="button" class="btn btn-outline btn-default" value="Add New" onclick="location.href='<?php echo base_url()?>admin/profilepicture/create';"/>
                            <h3 class="text-center">Block</h3>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables">
                                    <thead>
                                        <tr>
                                            <th>Sn</th>
                                            <th>attachment</th>
                                            <th>Status</th>
                                            <th class="edit">Manage</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                <?php $sno = 1;?>
               <?php foreach($query->result() as $row){?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><img src="<?php echo base_url();?>uploads/profilepicture/<?php echo $row->attachment;?>"height="25"width="25"></td>
                    <td><?php echo $row->status;?></td> 
                    <td class="edit">
                            <a href="<?php echo base_url()?>profilepicture/admin/create/<?php echo $row->id;?>"><img src="<?php echo base_url()?>nimesh/img/attendanceIconBtn.png"height="20" width="20" alt="carrers"/><i class="icon-pencil"></i></a>                   
                            &nbsp;&nbsp;/&nbsp;&nbsp; 
                            <a href="<?php echo base_url()?>profilepicture/admin/delete/<?php echo $row->id;?>" onclick="return confirm('Are you sure, you want to delete it?');"><img src="<?php echo base_url()?>nimesh/img/cancelIcon.png"height="20" width="20" alt="leasing"/><i class="icon-trash"></i></a>
                	</td> 
                </tr> 
                
             
                
				<?php }	?>            
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                          
                        </div>
                        <!-- /.panel-body -->
                    </div>	
							
						</div>
							<div id="QuickAccess" class="col-md-4 attendancesQuickAccess">
								<div class="panel panel-default site-panel">
									<div class="panel-body">
										<h3 class="text-center">Quick Access</h3>
										<div class="row col-md-offset-3">
											<button class="btn btn-default col-md-8" type="submit" name="addTeacher" onclick="location.href='<?php echo base_url()?>teacher/admin/create';">
												<p><img src="<?php echo base_url();?>nimesh/img/addTeacherIcon.png"></p>
												<p style="width:90%; margin:auto auto; color:green">Add Teacher</p>
											</button>
											<button class="btn btn-default col-md-8" type="submit" name="voidClass" onClick="#">
												<p><img src="<?php echo base_url();?>nimesh/img/voidClassIcon.png"></p>
												<p>Void Class</p>
											</button>
											<button class="btn btn-default col-md-8" type="submit" name="manageSchedule" onClick="#">
												<p><img src="<?php echo base_url();?>nimesh/img/manageScheduleIcon.png"></p>
												<p>Manage Schedule</p>
											</button>
											<button class="btn btn-default col-md-8" type="submit" name="viewReports" onClick="#">
												<p><img src="<?php echo base_url();?>nimesh/img/reportIcon.png"></p>
												<p>View Reports</p>
											</button>
										</div>
									</div>
								</div>
							</div><!--QuickAccess-->
						</div>
					</div>
				</div>
			</div><!--row-->
		</div><!--container-fluid -->
	</body>
	</html>

