<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		//$this->load->module('admin_login/admin_login');
		//$this->admin_login->check_session_and_permission('profilepicture'); //module name is groups here	
	}
	
	function index()
	{	
           
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
                $this->load->view("admin/table",$data);
                
                //above process can be done on this way also
                //$data['view_file']="admin/table";
               // echo Modules::run('template/admin_template',$data);
	}
	
//	function get_data_from_post()
//	{
//		$data['name'] = $this->input->post('name', TRUE);		
//		
//			$update_id = $this->input->post('update_id', TRUE);
//		return $data;
//	}
//	
        
        function get_data_from_post()
	{
		$data['attachment'] = $this->input->post('attachment', TRUE);
                $data['status'] = $this->input->post('status', TRUE);
		$update_id = $this->input->post('update_id', TRUE);
			$update_id = $this->input->post('update_id', TRUE);
                        if(is_numeric($update_id))
			{
                            $attach = $this->get_attachment_from_db($update_id);
				$data['attachment'] = $attach['attachment'];
                              	
			}
			else
			{ $data['attachment'] = $this->input->post('userfile', TRUE);
                        	
			}
                         
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['attachment'] = $row->attachment;
                        $data['status'] = $row->status;	

		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
//	function get_comments_from_db($update_id)
//	{
//		$query = $this->get_where($update_id);
//		foreach($profilepicture->result() as $row)
//		{			
//			$data['comments'] = $row->comments;				
//		}
//			return $data;
//	}
        function do_upload($id) 
	{ 
	   $config['upload_path']   =   "./uploads/profilepicture"; 
	   $config['file_name'] = $id;		   
	   $config['overwrite'] = TRUE;
	   $config['allowed_types'] =   "gif|jpg|jpeg|png";  
	   $config['max_size']      =   "20480"; //that's 20MB
	   $config['max_width']     =   "1907"; 
	   $config['max_height']    =   "1280"; 

	   $this->load->library('upload',$config);

		   
		if ( ! $this->upload->do_upload())
		{
			//echo 'File cannot be uploaded';
			$datas = array('error' => $this->upload->display_errors());
		}
		else
		{
			echo 'File has been uploaded';
                    
			$datas = array('upload_data' => $this->upload->data());
             	
		}
		
		return $datas;
	}
    	function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			
                    $data['attachment'] = $row->attachment;
                    	
                    
		}
			return $data;
	}
       
	
	function create()
	{
            
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
	
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
		
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
				
			$data['view_file'] = "admin/form";
			 $this->load->view("admin/form",$data);	
	}
	
          function submit()
        {   
            $data = $this->get_data_from_post();
            
            $update_id = $this->input->post('update_id', TRUE);
           if(is_numeric($update_id)){						
					
				$attach = $this->get_attachment_from_db($update_id);
				$uploadattachment = $this->do_upload($update_id);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				if(empty($data['attachment'])){
				$data['attachment'] = $attach['attachment'];}
				
				$this->_update($update_id, $data);
			} else {
                                  $nextid = $this->get_id('id');
                                  //print_r($nextid); die();
                                 //$nextid=2;
				$uploadattachment = $this->do_upload($nextid);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
                                $this->_insert($data);
            }
           redirect('profilepicture/admin');
        }	
     function get_id(){
	$this->load->model('mdl_profilepicture');
	$id = $this->mdl_profilepicture->get_id();
	return $id;
	}
	function get($order_by){
	$this->load->model('mdl_profilepicture');
	$query = $this->mdl_profilepicture->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_profilepicture');
	$query = $this->mdl_profilepicture->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_profilepicture');
	$this->mdl_profilepicture->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_profilepicture');
	$this->mdl_profilepicture->_update($id, $data);
	}
        
        function delete()
	{	$this->load->model('mdl_profilepicture');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/profilepicture');
			}
		else
		{
			$this->mdl_profilepicture->_delete($delete_id);
			redirect('admin/profilepicture');
		}			
			
	}
}