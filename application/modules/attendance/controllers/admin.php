<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		//$this->load->module('admin_login/admin_login');
		//$this->admin_login->check_session_and_permission('attendance'); //module name is groups here	
	}
	
	function index()
	{	
           
		$data['query'] = $this->get_student('id');
                $data['indexcount']=0;
//		var_dump($data); die;
		$data['view_file'] = "admin/table";
                $this->load->view("admin/table",$data);
                
	}
	function attendanceUpdate()
        {
//            var_dump($_POST);die;
            $data['indexcount'] = $_POST['indexcount']+1;
            $data['query'] = $this->get_student('id');
            if($data['indexcount']<count($data['query'])){
                //do insert
                $data['view_file'] = "admin/tableajax";
            }
            else{
               echo 'finished'; die;
            }
            
            $this->load->view("admin/tableajax",$data);
        }
//	function get_data_from_post()
//	{
//		$data['name'] = $this->input->post('name', TRUE);		
//		
//			$update_id = $this->input->post('update_id', TRUE);
//		return $data;
//	}
//	
        
        function get_data_from_post()
	{
		$data['firstname'] = $this->input->post('firstname', TRUE);
                $data['lastname'] = $this->input->post('lastname', TRUE);
                $data['intake_id'] = $this->input->post('intake_id', TRUE);
                $data['course_id'] = $this->input->post('course_id', TRUE);
                $data['phone_no'] = $this->input->post('phone_no', TRUE);
                $data['email'] = $this->input->post('email', TRUE);
                $data['status'] = $this->input->post('status', TRUE);
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['firstname'] = $row->firstname;
                        $data['lastname'] = $row->lastname;
                        $data['intake_id'] = $row->intake_id;
                        $data['course_id'] = $row->course_id;
                        $data['phone_no'] = $row->phone_no;
                        $data['email'] = $row->email;
                        $data['status'] = $row->status;	

		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
//	function get_comments_from_db($update_id)
//	{
//		$query = $this->get_where($update_id);
//		foreach($attendance->result() as $row)
//		{			
//			$data['comments'] = $row->comments;				
//		}
//			return $data;
//	}
	
	function create()
	{
            
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
	
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
		
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
				
			$data['view_file'] = "admin/form";
			 $this->load->view("admin/form",$data);	
	}
	
         function submit()
        {          
            $data = $this->get_data_from_post();
            
            $update_id = $this->input->post('update_id', TRUE);
            if(is_numeric($update_id)){				
                    $this->_update($update_id, $data);
            } else {
            
                    $this->_insert($data);
                   
            }
            redirect('admin/attendance');
        }
		

	function get($order_by){
	$this->load->model('mdl_attendance');
	$query = $this->mdl_attendance->get($order_by);
	return $query;
	}
        	function get_student($order_by){
	$this->load->model('student/mdl_student');
	$query = $this->mdl_student->get_student($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_attendance');
	$query = $this->mdl_attendance->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_attendance');
	$this->mdl_attendance->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_attendance');
	$this->mdl_attendance->_update($id, $data);
	}
        
        function delete()
	{	$this->load->model('mdl_attendance');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/attendance');
			}
		else
		{
			$this->mdl_attendance->_delete($delete_id);
			redirect('admin/attendance');
		}			
			
	}
}