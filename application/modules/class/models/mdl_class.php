 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_class extends CI_Model {

	function __construct() {
	parent::__construct();
	}

	function get_table() {
	$table = "ai_class";
	return $table;
	} 
       
        
	
        function get_front($order_by){
	$table = $this->get_table();
	$this->db->order_by($order_by);
        $this->db->where('status','live');
	$query=$this->db->get($table)->result();
	return $query;
	}
	function get($order_by){
	$table = $this->get_table();
	$this->db->order_by($order_by,'ASC');
	$query=$this->db->get($table);
	return $query;
	}
        function get_class_teacher(){
	$table = $this->get_table();
        $this->db->select('ai_class.*');
        $this->db->select('ai_student_group.title as student_group');
        $this->db->join('ai_student_group','ai_class.student_group_id=ai_student_group.id');
        $dropdowns = $this->db->get('ai_class')->result();
        $dropdownlist[0] = 'Select Class';
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->student_group;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
//                var_dump($finaldropdown); die;
		return $finaldropdown;
	}

        
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
        
	
	function _insert($data){
            
	$table = $this->get_table();
	$this->db->insert($table, $data);
	}
	
	function get_id(){
	$result = mysql_query("SHOW TABLE STATUS LIKE 'class'");
	$row = mysql_fetch_array($result);
	$nextId = $row['Auto_increment']; 
	return $nextId;
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
        
        function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	}
	
//	
//	function get_modules_dropdown()
//	{
//	$this->db->select('id, title');	
//	$this->db->order_by('title');
//	$dropdowns = $this->db->get('up_blog')->result();
//	foreach ($dropdowns as $dropdown)
//		{
//		$dropdownlist[$dropdown->title] = $dropdown->title;
//		}
//	if(empty($dropdownlist)){return NULL;}
//	$finaldropdown = $dropdownlist;
//	return $finaldropdown;
//	}
//	
	function get_groups_dropdown()
	{
		$this->db->select('id, title ');
		$this->db->order_by('id','DESC');
		$dropdowns = $this->db->get('ai_class')->result();
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->title] = $dropdown->title;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}

}