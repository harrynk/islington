<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		//$this->load->module('admin_login/admin_login');
		//$this->admin_login->check_session_and_permission('class'); //module name is groups here	
	}
	
	function index()
	{	
           
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
                $this->load->view("admin/table",$data);
                
                //above process can be done on this way also
                //$data['view_file']="admin/table";
               // echo Modules::run('template/admin_template',$data);
	}
	
//	function get_data_from_post()
//	{
//		$data['name'] = $this->input->post('name', TRUE);		
//		
//			$update_id = $this->input->post('update_id', TRUE);
//		return $data;
//	}
//	
        
        function get_data_from_post()
	{

                $data['date'] = $this->input->post('date', TRUE);
                $data['start_time'] = $this->input->post('start_time', TRUE);
                $data['end_time'] = $this->input->post('end_time', TRUE);
                $data['type'] = $this->input->post('type', TRUE);
                $data['subject_id'] = $this->input->post('subject_id', TRUE);
                $data['teacher_id'] = $this->input->post('teacher_id', TRUE);
                $data['student_group_id'] = $this->input->post('student_group_id', TRUE);
                $data['block_id'] = $this->input->post('block_id', TRUE);
                $data['venu_id'] = $this->input->post('venu_id', TRUE);
                $data['status'] = $this->input->post('status', TRUE);
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
                        $data['date'] = $row->date;
                        $data['start_time'] = $row->start_time;
                        $data['end_time'] = $row->end_time;
                        $data['type'] = $row->type;
                        $data['subject_id'] = $row->subject_id;
                        $data['teacher_id'] = $row->teacher_id;
                        $data['student_group_id'] = $row->student_group_id;
                        $data['block_id'] = $row->block_id;
                        $data['venu_id'] = $row->venu_id;
            $data['status'] = $row->status;	

		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
//	function get_comments_from_db($update_id)
//	{
//		$query = $this->get_where($update_id);
//		foreach($class->result() as $row)
//		{			
//			$data['comments'] = $row->comments;				
//		}
//			return $data;
//	}
	
	function create()
	{
            
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
	
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
                                
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
		
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
                        $data['subject_list']=$this->subject_list();
                        $data['teacher_list']=$this->teacher_list();
                        $data['studentgroup_list']=$this->studentgroup_list();
                        $data['block_list']=$this->block_list();
                         $data['venu_list']=$this->venu_list();
			$data['view_file'] = "admin/form";
			 $this->load->view("admin/form",$data);	
	}
	
         function submit()
        {          
            $data = $this->get_data_from_post();
            $update_id = $this->input->post('update_id', TRUE);
            if(is_numeric($update_id)){				
                    $this->_update($update_id, $data);
            } else {
            
                    $this->_insert($data);
                   
            }
            redirect('admin/class');
        }
		

	function get($order_by){
	$this->load->model('mdl_class');
	$query = $this->mdl_class->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_class');
	$query = $this->mdl_class->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_class');
	$this->mdl_class->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_class');
	$this->mdl_class->_update($id, $data);
	}
        	function subject_list() {
        $this->load->model('subject/mdl_subject');
        $query = $this->mdl_subject->get_groups_dropdown();
        if (empty($query)) {
            return NULL;
        }
        return $query;
    }
       
    function teacher_list() {
        $this->load->model('teacher/mdl_teacher');
        $query = $this->mdl_teacher->get_groups_dropdown1();
        if (empty($query)) {
            return NULL;
        }
        return $query;
    }
     function studentgroup_list() {
        $this->load->model('student_group/mdl_student_group');
        $query = $this->mdl_student_group->get_groups_dropdown2();
        if (empty($query)) {
            return NULL;
        }
        return $query;
    }
     
        function block_list() {
        $this->load->model('block/mdl_block');
        $query = $this->mdl_block->get_groups_dropdown3();
        if (empty($query)) {
            return NULL;
        }
        return $query;
    }
    function venu_list(){
        $this->load->model('venu/mdl_venu');
         $query = $this->mdl_venu->get_groups_dropdown3();
        if (empty($query)) {
            return NULL;
        }
        return $query;
        
    }
        function delete()
	{	$this->load->model('mdl_class');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/class');
			}
		else
		{
			$this->mdl_class->_delete($delete_id);
			redirect('admin/class');
		}			
			
	}
}