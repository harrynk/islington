<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Reports-RTE-Attendance de' Islington</title>
	<link rel="shortcut icon" href="<?php echo base_url();?>nimesh/img/titleLogo.png">
        	<script src="<?php echo base_url();?>nimesh/js/bootstrap/jquery.js" rel="stylesheet"></script>
	<link href="<?php echo base_url();?>nimesh/css/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>nimesh/css/rte profile/rte_view_attendance_bootstrap.css" rel="stylesheet">
</head>
<body id="mainBody">
	<div class="container-fluid">
		<div class="row">
			<div id="sideSection" class="col-sm-12 col-sx-12 col-md-3 col-lg-3">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div id="profilePic" class="col-md-8 col-md-offset-2">
								<figure id="tm_pp" class="thumbnail logo-thumbnail">
									<img src="<?php echo base_url();?>nimesh/img/profilePicture.jpg"/> 
								</figure>
							</div>
						</div>
						<div class="row text-center blue-text">
							<span class="glyphicon glyphicon-edit" aria-hidden="true"> Edit Picture</span>
						</div>
						<div class="row text-center blue-text">
							<button class="btn btn-default">Chose file</button>
						</div>
						<ul class="list-group">
							<li class="navSidebar list-group-item">
								<a href="#">My Dashboard</a>
							</li>
							<li class="navSidebar list-group-item">
								<a style="color:#E70F12;">View Attendance</a>
							</li>
							
							<li class="navSidebar list-group-item">
								<a href="#">Edit My Profile</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Log Out</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-lg-9">
				<div id="header" class="row">
					<div id="headings" class="col-md-9 text-center">
						<h1 class="blue-text">ATTENDANCE DE' ISLINGTON</h1>
						<h3 class="red-text">Reports - Teacher</h3>
					</div>
					<div id="logoContainer" class="col-md-3">
						<figure id="appLogo" class="thumbnail logo-thumbnail">
							<img src="<?php echo base_url();?>nimesh/img/logo.png"/>
						</figure>
					</div>
				</div>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol>
				<div class="row">
					<div id="middleSection">
						<div id="reportGeneration" class="col-md-8"> 
							<div class="panel panel-default site-panel">
								<div class="panel-body">
									
									<div class="row">
										
										
											<div class="form-group">
                                                                                            <form class="form-horizontal col-md-offset-2">
												<h5> <?php echo  date('l jS \of F Y h:i:s A');?></h5>
												<div class="col-md-6" id="replaceable">
												<div class="col-md-12">
													Please select class and subject.<br>
                                                                                                        <strong>Start the attendance.</strong>
                                                                                                        <figure id="appLogo" class="thumbnail logo-thumbnail">
							<img src="<?php echo base_url();?>nimesh/img/logo.png"/>
						</figure>
												</div>
												
												
											</div>
										</form>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<div id="QuickAccess" class="col-md-4 attendancesQuickAccess">
						<div class="panel panel-default site-panel">
							<div class="panel-body">
							</br>
							</br>
							<h6 class="text-center">Select Class</h6>
									<div class="row-md-6">
                                                                            <?php
                                                                                                            $selected = '';
                                                                                                            $options = $class_list;
                                                                                                            echo form_dropdown('class_id', $options, $selected, 'class="form-control" id="class_id"');
                                                                                                            ?>
												</div>
												<h6 class="text-center">Course module</h6>
												<div class="row-md-6">
                                                                                                    <?php
                                                                                                    $selected = '';
                                                                                                            $options = $course_list;
                                                                                                            echo form_dropdown('subject_id', $options, $selected, 'class="form-control" id="subject_id"');
                                                                                                            ?>
												</div>
								
							</div>
						</div>
					</div><!--QuickAccess-->
				</div>
			</div>
		</div><!--row-->
	</div><!--container-fluid -->
</body>
</html>
<script>
    
    $('#class_id').change(function(){
        var class_id=$('#class_id').val();
           $.ajax({
        type:'POST',
        url:'student/get_student_list',
        data:"class_id=" + class_id, 
        success:function(data)
        {
           $('#replaceable').html(data); 
        }
    });
    });
    
</script>