<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MX_Controller {

    public function __construct() {
        parent::__construct();
        //always check if session userdata value "logged_in" is not true
        if (!$this->session->userdata("logged_in"))/* every logged_in user get privilage to access dashboard */ {
            redirect('admin');
        }
    }

    function index() {//this is admin/dashboard(check config/routes.php to be clear
        $group_id = $this->session->userdata('group_id');
        if ($group_id == 1)
            $data['view_file'] = "admin/home";
        else {
            $data['class_list']=$this->get_class();
            $data['course_list']=$this->get_subject();
            $data['view_file'] = "admin/teacher";
        }
        $this->load->view($data['view_file'],$data);
//		$this->admin_template->admin($data);	
    }
    function get_class(){
       $this->load->model('class/mdl_class') ;
      $result= $this->mdl_class->get_class_teacher();
      return $result;
    }
    function get_subject(){
        $this->load->model('subject/mdl_subject') ;
      $result= $this->mdl_subject->get_groups_dropdown();
      return $result;
    }

}
