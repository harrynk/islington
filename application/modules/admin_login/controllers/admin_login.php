<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_login extends MX_Controller
{

	function __construct() {
	parent::__construct();
	}
	
	function index()
	{		
		$this->check_session();	
	}	
	
	function login(){
		$data['view_file'] = "loginform";
		$this->load->module('template');
		$this->template->userlogin($data);		
	}
	

	function submit()
	{
//            var_dump($_POST); die;
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'Username', 'required|max_length[100]|xss_clean');
		$this->form_validation->set_rules('pword', 'Password', 'required|max_length[100]|xss_clean');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->login();
		}
		else
		{
			extract($_POST);
			$this->load->model('mdl_admin_login');
			$ids = $this->mdl_admin_login->check_login($username, $pword);
			if(empty($ids)){
				$user_id = $group_id = '';
				}
			else{
			$user_id = $ids->id;
			$group_id = $ids->group_id;
				}
						
			if(!$user_id && !$group_id)
			{
				$this->session->set_userdata('login_error', TRUE);
				$this->login();			
			}
			else
			{				
				$this->session->set_userdata(array(
				'logged_in' => TRUE,
				'user_id' => $user_id,
				'group_id' => $group_id,
				'permission' => $access
				));	
                
				       
				$this->session->unset_userdata('login_error');
				redirect('admin/dashboard');
			
			}			
		}
	}	
	
	function logout()
	{
		
	$this->session->unset_userdata('logged_in');
	$this->session->unset_userdata('user_id');	
	$this->session->unset_userdata('group_id');	
	redirect('admin');	
			
	}
	
	function check_session()
	{
		if($this->session->userdata('logged_in'))/*to check session and make sure that it has only admin privilage*/
		{
			redirect('admin/dashboard');
		}
		else
		{
			$this->login();
		}	
	}
	
	
	function check_session_and_permission($modulename)
	{
		$group_id = $this->session->userdata("group_id");
		$access = $this->get_all_permissions($group_id);
		
	 	if($this->session->userdata("logged_in")){
			
			if($access == 'granted'){	
			}
			elseif($access == 'denied'){	
				redirect('admin');	
			}
			else{	
				$moduleid = $this->get_id_from_modulename($modulename);
				if(!isset($access[$moduleid])){
					redirect('admin/dashboard');	
				}				
			}	
			
			//redirect('admin/dashboard');
		}
	}
	
	
	function get_all_permissions($group_id){	
	
				/*checkin permission*/		
				if($group_id != 1){ //for non-admin
					$permission_status = $this->check_permission_existence_other_than_admin($group_id);//finish this
			
					if($permission_status == TRUE){//finish this
						$access = $this->unserialize_role_array($group_id);	//finish this
					}else {		
						$access = 'denied';				
					}//finish this                        					
				}
				else //for admin
				{
				$access = 'granted';
				}
	
		return $access;
	}
	
	function get_id_from_modulename($modulename){
	$this->load->model('modules/mdl_moduleslist');
	$query = $this->mdl_moduleslist->get_id_from_modulename($modulename);
	return $query;			
	}	
	
	function check_permission_existence_other_than_admin($group_id){
	$this->load->model('permissions/mdl_permissions');
	$query = $this->mdl_permissions->check_permission_existence($group_id);
	return $query;		
	}	
	
	function unserialize_role_array($group_id){		
	$this->load->model('permissions/mdl_permissions');
	$array = $this->mdl_permissions->unserialize_role_array($group_id);
	return $array;	
	}
	
	
	
	function get($order_by){
	$this->load->model('mdl_admin_login');
	$query = $this->mdl_admin_login->get($order_by);
	return $query;
	}
	
	function get_with_limit($limit, $offset, $order_by) {
	$this->load->model('mdl_admin_login');
	$query = $this->mdl_admin_login->get_with_limit($limit, $offset, $order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_admin_login');
	$query = $this->mdl_admin_login->get_where($id);
	return $query;
	}
	
	function get_where_custom($col, $value) {
	$this->load->model('mdl_admin_login');
	$query = $this->mdl_admin_login->get_where_custom($col, $value);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_admin_login');
	$this->mdl_admin_login->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_admin_login');
	$this->mdl_admin_login->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_admin_login');
	$this->mdl_admin_login->_delete($id);
	}
	
	function count_where($column, $value) {
	$this->load->model('mdl_admin_login');
	$count = $this->mdl_admin_login->count_where($column, $value);
	return $count;
	}

	function get_max() {
	$this->load->model('mdl_admin_login');
	$max_id = $this->mdl_admin_login->get_max();
	return $max_id;
	}

	function _custom_query($mysql_query) {
	$this->load->model('mdl_admin_login');
	$query = $this->mdl_admin_login->_custom_query($mysql_query);
	return $query;
	}

}