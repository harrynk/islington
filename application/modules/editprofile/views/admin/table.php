<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Reports-RTE-Attendance de' Islington</title>
	<link rel="shortcut icon" href="<?php echo base_url();?>nimesh/img/titleLogo.png">
	<link href="<?php echo base_url();?>nimesh/css/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>nimesh/css/rte profile/rte_view_attendance_bootstrap.css" rel="stylesheet">
</head>
<body id="mainBody">
	<div class="container-fluid">
		<div class="row">
			<div id="sideSection" class="col-sm-12 col-sx-12 col-md-3 col-lg-3">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div id="profilePic" class="col-md-8 col-md-offset-2">
								<figure id="tm_pp" class="thumbnail logo-thumbnail">
									<img src="<?php echo base_url();?>nimesh/img/profilePicture.jpg"/> 
								</figure>
							</div>
						</div>
						<div class="row text-center blue-text">
							<span class="glyphicon glyphicon-edit" aria-hidden="true"> Edit Picture</span>
						</div>
						<div class="row text-center blue-text">
							<button class="btn btn-default">Chose file</button>
						</div>
						<ul class="list-group">
							<li class="navSidebar list-group-item">
								<a href="#">My Dashboard</a>
							</li>
							<li class="navSidebar list-group-item">
								<a style="color:#E70F12;">View Attendance</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Void Class</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">View Reports</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Add Teacher</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Edit Teacher Profile</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Manage Schedule</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Manage Groups</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Import Students</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Edit My Profile</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Log Out</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-lg-9">
				<div id="header" class="row">
					<div id="headings" class="col-md-9 text-center">
						<h1 class="blue-text">ATTENDANCE DE' ISLINGTON</h1>
						<h3 class="red-text">Reports - RTE</h3>
					</div>
					<div id="logoContainer" class="col-md-3">
						<figure id="appLogo" class="thumbnail logo-thumbnail">
							<img src="<?php echo base_url();?>nimesh/img/logo.png"/>
						</figure>
					</div>
				</div>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol>
				<div class="row">
					<div id="middleSection">
						<div id="rteHome" class="col-md-8"> 
							<div class="panel panel-default site-panel">
								<div class="panel-body">
									<h3 class="text-center">Home</h3>
									
									<form class="form-horizontal col-md-offset-2">
										<div class="form-group">
										      <h3> Change Username</h3>
											<label class="col-md-4 control-label blue-text">Current Username:</label>
											<div class="col-md-6">
												<input type="text" class="form-control" placeholder="current username">
											</div>
											<label class="col-md-4 control-label blue-text">New Username:</label>
											<div class="col-md-6">
												<input type="text" class="form-control" placeholder="new username">
											</div>
											<button> Change Username</button>
											<button> cancel</button>
											<div class=" horizontal divider"></div>
											<h3> Change Password</h3>
											<label class="col-md-4 control-label blue-text">Current Password </label>
											<div class="col-md-6">
												<input type="text" class="form-control" placeholder="current password">
											</div>
											<label class="col-md-4 control-label blue-text">New Password:</label>
											<div class="col-md-6">
												<input type="text" class="form-control" placeholder="new password">
											</div>
											<label class="col-md-4 control-label blue-text">Re Enter Password:</label>
											<div class="col-md-6">
												<input type="text" class="form-control" placeholder="reenter password">
											</div>
											<button> Change Password</button>
											<button> cancel</button>
										</div>
									</form>
								</div>
							</div>
						</div>
							<div id="QuickAccess" class="col-md-4 attendancesQuickAccess">
								<div class="panel panel-default site-panel">
									<div class="panel-body">
										<h3 class="text-center">Quick Access</h3>
										<div class="row col-md-offset-3">
											<button class="btn btn-default col-md-8" type="submit" name="addTeacher" onClick="#">
												<p><img src="<?php echo base_url();?>nimesh/img/addTeacherIcon.png"></p>
												<p style="width:90%; margin:auto auto; color:green">Add Teacher</p>
											</button>
											<button class="btn btn-default col-md-8" type="submit" name="voidClass" onClick="#">
												<p><img src="<?php echo base_url();?>nimesh/img/voidClassIcon.png"></p>
												<p>Void Class</p>
											</button>
											<button class="btn btn-default col-md-8" type="submit" name="manageSchedule" onClick="#">
												<p><img src="<?php echo base_url();?>nimesh/img/manageScheduleIcon.png"></p>
												<p>Manage Schedule</p>
											</button>
											<button class="btn btn-default col-md-8" type="submit" name="viewReports" onClick="#">
												<p><img src="<?php echo base_url();?>nimesh/img/reportIcon.png"></p>
												<p>View Reports</p>
											</button>
										</div>
									</div>
								</div>
							</div><!--QuickAccess-->
						</div>
					</div>
				</div>
			</div><!--row-->
		</div><!--container-fluid -->
	</body>
	</html>
