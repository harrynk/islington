 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_student extends CI_Model {

	function __construct() {
	parent::__construct();
	}

	function get_table() {
	$table = "ai_student";
	return $table;
	} 
       
        
	
        function get_front($order_by){
	$table = $this->get_table();
	$this->db->order_by($order_by);
        $this->db->where('status','live');
	$query=$this->db->get($table)->result();
        
	return $query;
	}
	function get($order_by){
	$table = $this->get_table();
	$this->db->order_by($order_by,'ASC');
	$query=$this->db->get($table);
	return $query;
	}
        function get_student_list_where($group_id){
	$table = $this->get_table();
        $this->db->where('student_group_id',$group_id);
	$this->db->order_by('id','ASC');
	$query=$this->db->get($table)->result_array();
	return $query;
	}


        
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
        
	
	function _insert($data){
	$table = $this->get_table();
	$this->db->insert($table, $data);
	}
	
	function get_id(){
	$result = mysql_query("SHOW TABLE STATUS LIKE 'student'");
	$row = mysql_fetch_array($result);
	$nextId = $row['Auto_increment']; 
	return $nextId;
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
        
        function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	}
	
//	
//	function get_modules_dropdown()
//	{
//	$this->db->select('id, title');	
//	$this->db->order_by('title');
//	$dropdowns = $this->db->get('up_blog')->result();
//	foreach ($dropdowns as $dropdown)
//		{
//		$dropdownlist[$dropdown->title] = $dropdown->title;
//		}
//	if(empty($dropdownlist)){return NULL;}
//	$finaldropdown = $dropdownlist;
//	return $finaldropdown;
//	}
//	
	function get_groups_dropdown()
	{
		$this->db->select('id, title ');
		$this->db->order_by('id','DESC');
		$dropdowns = $this->db->get('ai_student')->result();
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->title] = $dropdown->title;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}

}