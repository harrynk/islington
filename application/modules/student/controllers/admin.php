<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		//$this->load->module('admin_login/admin_login');
		//$this->admin_login->check_session_and_permission('student'); //module name is groups here	
	}
	
	function index()
	{	
           
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
                $this->load->view("admin/table",$data);
                
                //above process can be done on this way also
                //$data['view_file']="admin/table";
               // echo Modules::run('template/admin_template',$data);
	}
        function get_student_list(){
            if(!isset($_POST['std_id']))
            {
            
            }
            else {
                $data['indexcount'] = $_POST['indexcount'];
            }
            if(!isset($data['indexcount'])){
                $data['indexcount']=0;
            }
            else{
               $data['indexcount'] = $_POST['indexcount']+1; 
            }
            $data['class_id']=$_POST['class_id'];
            $this->load->model('mdl_student');
            $data['result']=$this->mdl_student->get_student_list_where($data['class_id']);
            if(isset($_POST['subject_id']))
                {
                    $this->insert_attendance($data);
                }
            if($data['indexcount']<count($data['result'])){
                
                $data['view_file'] = "admin/tableajax";
            }
            else{
               echo 'Attaendance Finished For Today'; die;
            }
            $this->load->view($data['view_file'],$data);
            
            
        }
	
//	function get_data_from_post()
//	{
//		$data['name'] = $this->input->post('name', TRUE);		
//		
//			$update_id = $this->input->post('update_id', TRUE);
//		return $data;
//	}
//	
        function insert_attendance($data)
	{
            $data_insert['student_id']=$_POST['std_id'];
            $data_insert['class_id']=$_POST['class_id'];
            $data_insert['subject_id']=$_POST['subject_id'];
            $data_insert['date']=date('Y-m-d');
            $data_insert['status']=$_POST['status'];
            if($data_insert['status']=='present')
            {
                $data_insert['status']='p';
            }
 else {
     $data_insert['status']='a';
 }
            $this->load->model('attendance/mdl_attendance');
            $this->mdl_attendance->_insert($data_insert);
        }
        function get_data_from_post()
	{
		$data['firstname'] = $this->input->post('firstname', TRUE);
                $data['lastname'] = $this->input->post('lastname', TRUE);
                $data['intake_id'] = $this->input->post('intake_id', TRUE);
                $data['course_id'] = $this->input->post('course_id', TRUE);
                $data['phone_no'] = $this->input->post('phone_no', TRUE);
                $data['email'] = $this->input->post('email', TRUE);
                $data['status'] = $this->input->post('status', TRUE);
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['firstname'] = $row->firstname;
                        $data['lastname'] = $row->lastname;
                        $data['intake_id'] = $row->intake_id;
                        $data['course_id'] = $row->course_id;
                        $data['phone_no'] = $row->phone_no;
                        $data['email'] = $row->email;
                        $data['status'] = $row->status;	

		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
//	function get_comments_from_db($update_id)
//	{
//		$query = $this->get_where($update_id);
//		foreach($student->result() as $row)
//		{			
//			$data['comments'] = $row->comments;				
//		}
//			return $data;
//	}
	
	function create()
	{
            
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
	
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
		
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
			$data['intake_list']=$this->intake_list();
                        $data['course_list']=$this->course_list();
                         $data['student_group_id_list']=$this->student_group_id_list();
			$data['view_file'] = "admin/form";
			 $this->load->view("admin/form",$data);	
	}
	
         function submit()
        {          
            $data = $this->get_data_from_post();
            
            $update_id = $this->input->post('update_id', TRUE);
            if(is_numeric($update_id)){				
                    $this->_update($update_id, $data);
            } else {
            
                    $this->_insert($data);
                   
            }
            redirect('admin/student');
        }
		
 function intake_list() {
        $this->load->model('intake/mdl_intake');
        $query = $this->mdl_intake->get_groups_dropdown();
        if (empty($query)) {
            return NULL;
        }
        return $query;
    }
     function course_list() {
        $this->load->model('course/mdl_course');
        $query = $this->mdl_course->get_groups_dropdown();
        if (empty($query)) {
            return NULL;
        }
        return $query;
    }
     function student_group_id_list() {
        $this->load->model('student_group/mdl_student_group');
        $query = $this->mdl_student_group->get_groups_dropdown2();
        if (empty($query)) {
            return NULL;
        }
        return $query;
    }
	function get($order_by){
	$this->load->model('mdl_student');
	$query = $this->mdl_student->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_student');
	$query = $this->mdl_student->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_student');
	$this->mdl_student->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_student');
	$this->mdl_student->_update($id, $data);
	}
        
        function delete()
	{	$this->load->model('mdl_student');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/student');
			}
		else
		{
			$this->mdl_student->_delete($delete_id);
			redirect('admin/student');
		}			
			
	}
}