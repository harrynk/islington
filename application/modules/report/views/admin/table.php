<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Reports-RTE-Attendance de' Islington</title>
	<link rel="shortcut icon" href="<?php echo base_url();?>nimesh/img/titleLogo.png">
	<link href="<?php echo base_url();?>nimesh/css/bootstrap/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>nimesh/css/rte profile/rte_view_attendance_bootstrap.css" rel="stylesheet">
        <!--script for datatable-->
<script type="text/javascript" src="<?php echo base_url();?>design/admin/js/jquery.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>design/dataTable/jquery.dataTables.min.css" >
<script type="text/javascript" src="<?php echo base_url();?>design/dataTable/jquery.dataTables.min.js"></script>
<!--end script for datatable-->
 
         <script>
    $(document).ready(function(){
        $('#dataTables').dataTable();
    });
    </script>
</head>
<body id="mainBody">
	<div class="container-fluid">
		<div class="row">
			<div id="sideSection" class="col-sm-12 col-sx-12 col-md-3 col-lg-3">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div id="profilePic" class="col-md-8 col-md-offset-2">
								<figure id="tm_pp" class="thumbnail logo-thumbnail">
									<img src="<?php echo base_url();?>nimesh/img/profilePicture.jpg"/> 
								</figure>
							</div>
						</div>
						<div class="row text-center blue-text">
							<span class="glyphicon glyphicon-edit" aria-hidden="true"> Edit Picture</span>
						</div>
						<div class="row text-center blue-text">
							<button class="btn btn-default">Chose file</button>
						</div>
						<ul class="list-group">
							<li class="navSidebar list-group-item">
								<a href="#">My Dashboard</a>
							</li>
							<li class="navSidebar list-group-item">
								<a style="color:#E70F12;">View Attendance</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Void Class</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">View Reports</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Add Teacher</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Edit Teacher Profile</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Manage Schedule</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Manage Groups</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Import Students</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Edit My Profile</a>
							</li>
							<li class="navSidebar list-group-item">
								<a href="#">Log Out</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-9 col-lg-9">
				<div id="header" class="row">
					<div id="headings" class="col-md-9 text-center">
						<h1 class="blue-text">ATTENDANCE DE' ISLINGTON</h1>
						<h3 class="red-text">Reports - RTE</h3>
					</div>
					<div id="logoContainer" class="col-md-3">
						<figure id="appLogo" class="thumbnail logo-thumbnail">
							<img src="<?php echo base_url();?>nimesh/img/logo.png"/>
						</figure>
					</div>
				</div>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li><a href="#">Library</a></li>
					<li class="active">Data</li>
				</ol>
				<div class="row">
					<div id="middleSection">
						<div id="reportGeneration" class="col-md-8"> 
							<div class="panel panel-default site-panel">
								<div class="panel-body">
									<h3 class="text-center">Report Generation</h3>
									<div class="row">
										<form class="form-horizontal col-md-offset-2">
											<div class="form-group">
												<label class="col-md-4 control-label blue-text">Select Type - </label>
												<div class="col-md-6">
													<select class="form-control" id="reportSelectType" name="reportSelectType">
														<option  value= "absentees">&nbsp;Absentees</option> 
														<option  value= "presents">&nbsp;Presents</option>
														<option  value= "both">&nbsp;Both</option>
													</select>
												</div>
												<label class="col-md-4 control-label blue-text">Select Date - </label>
												<div class="col-md-6">
													<select class="form-control" id="reportSelectDate" name="reportSelectDate">
														<option  value= "today">&nbsp;Today</option> 
														<option  value= "thisWeek">&nbsp;This Week</option>
														<option  value= "thisMonth">&nbsp;This Month</option>
														<option  value= "thisSemester">&nbsp;This Semester</option>
														<option  value= "thisYear">&nbsp;This Year</option>
													</select>
												</div>
												<label class="col-md-4 control-label blue-text">Select Class - </label>
												<div class="col-md-6">
													<select class="form-control" id="reportSelectClass" name="reportSelectClass">
														<option  value= "y1c">&nbsp;Year 1 Computing</option> 
														<option  value= "y1m">&nbsp;Year 1 Multimedia</option>
														<option  value= "y1n">&nbsp;Year 1 Networking</option>
														<option  value= "y2c">&nbsp;Year 2 Computing</option> 
														<option  value= "y2m">&nbsp;Year 2 Multimedia</option>
														<option  value= "y2n">&nbsp;Year 2 Networking</option>
														<option  value= "y3c">&nbsp;Year 3 Computing</option> 
														<option  value= "y3m">&nbsp;Year 3 Multimedia</option>
														<option  value= "y3n">&nbsp;Year 3 Networking</option>
													</select>
												</div>
												<label class="col-md-4 control-label blue-text">Select Group - </label>
												<div class="col-md-6">
													<select class="form-control" id="reportSelectGroup" name="reportSelectGroup">
														<option  value= "L1C1">&nbsp;L1C1</option> 
														<option  value= "L1C2">&nbsp;L1C2</option>
														<option  value= "L1C3">&nbsp;L1C3</option>
														<option  value= "L1M1">&nbsp;L1M1</option>
														<option  value= "L1M2">&nbsp;L1M2</option>
														<option  value= "L1N1">&nbsp;L1N1</option>
														<option  value= "L1N2">&nbsp;L1N2</option>
														<option  value= "L2C1">&nbsp;L2C1</option>
														<option  value= "L2C2">&nbsp;L2C2</option>
														<option  value= "L2C3">&nbsp;L2C3</option>
														<option  value= "L2C4">&nbsp;L2C4</option>
														<option  value= "L2N1">&nbsp;L2N1</option>
														<option  value= "L2N2">&nbsp;L2N2</option>
														<option  value= "L2M1">&nbsp;L2M1</option>
														<option  value= "L3C1">&nbsp;L3C1</option>
														<option  value= "L3C2">&nbsp;L3C2</option>
														<option  value= "L3C3">&nbsp;L3C3</option>
														<option  value= "L3M1">&nbsp;L3M1</option>
														<option  value= "L3M2">&nbsp;L3M2</option>
														<option  value= "L3M3">&nbsp;L3M3</option>
														<option  value= "L3N1">&nbsp;L3N1</option>
														<option  value= "L3N2">&nbsp;L3N2</option>              
													</select>
												</div>
											</div>
										</form>
									</div>
									<div class="row">
										<p class="col-md-offset-5" ><input type="checkbox" type="checkbox" name="chkboxDismissedClass">&nbsp;Insert Dismissed Class Details</p>
										<p class="col-md-offset-5">
											<button class="btn btn-default" type="submit" name="btnViewAttendances">
												<img src="<?php echo base_url();?>nimesh/img/attendanceIconBtn.png">
												Generate report
											</button>
											<button class="btn btn-default" type="reset" name="cancel">
												<img src="<?php echo base_url();?>nimesh/img/cancelIcon.png">
												Cancel
											</button>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div id="QuickAccess" class="col-md-4 attendancesQuickAccess">
						<div class="panel panel-default site-panel">
							<div class="panel-body">
								<h3 class="text-center">Quick Access</h3>
								<div class="row col-md-offset-3">
									<button class="btn btn-default col-md-8" type="submit" name="addTeacher" onClick="#">
										<p style="width:2em; margin:auto auto;"><img src="<?php echo base_url();?>nimesh/img/addTeacherIcon.png"></p>
										<p style="width:90%; margin:auto auto; color:green">Add Teacher</p>
									</button>

									<button class="btn btn-default col-md-8" type="submit" name="voidClass" onClick="#">
										<p style="width:2em; margin:auto auto;"><img src="<?php echo base_url();?>nimesh/img/voidClassIcon.png"></p>
										<p style="width:90%; margin:auto auto; color:green">Void Class</p>
									</button>

									<button class="btn btn-default col-md-8" type="submit" name="manageSchedule" onClick="#">
										<p style="width:2em; margin:auto auto;"><img src="<?php echo base_url();?>nimesh/img/manageScheduleIcon.png"></p>
										<p style="width:100%; margin:auto auto; color:green">Manage Schedule</p>
									</button>

									<button class="btn btn-default col-md-8" type="submit" name="viewReports" onClick="#">
										<p style="width:2em; margin:auto auto;"><img src="<?php echo base_url();?>nimesh/img/reportIcon.png"></p>
										<p style="width:90%; margin:auto auto; color:green">View Attendance</p>
									</button>
								</div>
							</div>
						</div>
					</div><!--QuickAccess-->
				</div>
			</div>
		</div><!--row-->
	</div><!--container-fluid -->
</body>
</html>