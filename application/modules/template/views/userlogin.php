<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Login-Attendance de' Islington</title>
<link rel="shortcut icon" href="<?php echo base_url();?>nimesh/img/titleLogo.png">
<link href="<?php echo base_url();?>nimesh/css/bootstrap/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>nimesh/css/login/login.css" rel="stylesheet">
<style>
.error{
text-align: center;
color:red;
font-size: 12px;
}
</style>

</head>

<body id="loginBody">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3 col-lg-3">
                <div class="thumbnail logo-thumbnail">
                    <img src="<?php echo base_url();?>nimesh/img/logo.png">
                </div>
            </div>
            <div class="col-md-8 text-center">
                <h1 class="blue-text">ATTENDENE DE' ISLINGTON</h1>
                <h3 class="red-text">System Login</h3>
            </div>
        </div>
        <div class="col-md-offset-4 col-md-6">
            <div class="panel panel-default panel-login">
                <div class="panel-body">
                    <legend class="text-center">System Login - Islington</legend>
                    <form action="admin_login/submit" method="post" class="form-horizontal col-md-offset-2 col-md-8">
                    <div class="form-group">
                        <label for="username" class="col-sm-2 control-label blue-text">Username: </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="username" name="username" placeholder="username">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label blue-text">Password:</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="pword" placeholder="password">
                        </div>
                    </div>
                    <div class="form-group">
                    <div class="col-sm-offset-2 checkbox">
                        <label><input type="checkbox">Keep me logged in</label>
                    </div>
                        <div class="col-sm-offset-2">
                          <button type="submit" class="btn btn-primary">Sign in</button>
                          <button type="submit" class="btn btn-danger">Cancel</button>
                        </div>
                      </div>
                    </form>  
                </div>
            </div>
        </div>
    </div>
</body>

</html>




