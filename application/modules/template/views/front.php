<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $site_settings['site_name'];?></title>
<meta name="title" content="<?php echo $site_settings['site_name'];?>">
<meta name="keywords" content="<?php echo $site_settings['meta_topic'];?>">
<meta name="description" content="<?php echo $site_settings['meta_data'];?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="<?php echo base_url();?>uploads/settings/<?php echo $site_settings['favicon'];?>">

<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/normalize.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/responsive-slider.css">

<script src="<?php echo base_url();?>design/frontend/js/jquery.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/vendor/modernizr-2.6.2.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">    
        <div id="heading_top">
          <div class="row">
            <div class="col-lg-2"> 
                <a title="Upveda Technology Pvt. Ltd, Nepal" href="#"><img src="<?php echo base_url();?>uploads/settings/<?php echo $site_settings['logo'];?>" alt="SERDeN logo" height="80" style="text-align:center;"></a>
             </div>
            <div class="col-lg-8">
                <div class="logo_info_title">
                	Upveda Technology CMS Template
                </div>	
                <div class="logo_info">
                	A CMS built for your ease.
                </div>
            </div>
            <div class="col-lg-2">
                <ul class="icons">
                    <li class="icon"><a href="http://twitter.com"><img src="<?php echo base_url();?>design/frontend/img/twitter.jpg"></a></li>
                    <li class="icon"><a href="http://facebook.com"><img src="<?php echo base_url();?>design/frontend/img/facebook.png"></a></li>
                    <li class="icon"><a href="https://plus.google.com"><img src="<?php echo base_url();?>design/frontend/img/google.jpg"></a></li>
                </ul>
            </div>
          </div>
        </div>
    	<div class="clearfix"></div>
    	<!--navigation bar------------------------------------------------------------>
        
        <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>
                

                
                <div class="navbar-collapse collapse" style="height: 1px;">
                  <ul class="nav navbar-nav">
                  
                    <?php foreach($headernav as $header){//still need to work on slug?> 
                    <?php if(empty($header['children'])){ echo '<li>';/*this is for no-child*/} else{?>                    	
                    <li class="dropdown">
                    <?php }/*this is for dropdown*/?>
                    
							<?php if(!empty($header['children'])){/*this is for dropdown*/?> 
                            	<?php if($header['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                <a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $header['title']?>                                    <b class="caret"></b>                                
                            <?php }/*end of if*/
                            else { /*this is for no-child*/?>                            
                            	<?php if($header['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                <a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>"><?php echo $header['title']?>
                            <?php } ?>                           
                                </a>
                                    
									<?php if(!empty($header['children'])){/*this is for dropdown*/?> 
                                    <ul class="dropdown-menu">
                                        <?php foreach($header['children'] as $child){ ?>  
                                            <li>                                            
                            					<?php if($child['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                                <a href="<?php echo $prefix.$child['href']; ?>" target="<?php echo $child['target']?>"><?php echo $child['title']; ?></a>
                                            </li>
                                        <?php }/*end of child foreach*/ ?>
                                    </ul>
                                    <?php } ?>  
                        
                    </li>
                    <?php }/*end of parent foreach*/ ?>
                    
                  </ul>
                </div><!--/.nav-collapse -->
              </div><!--banner-------------------------------------------------------------------->
              
    				<?php 
					$first_bit = $this->uri->segment(1);
					if($first_bit=="" || $first_bit == "home"){?>
									
								
				<!--============================================start of responsive slider============================-->
					
						<div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
					<div class="slides" data-group="slides">
						<ul>
							<?php foreach($banner as $row){ //displaying all banner ?> 
								<li>
								  <div class="slide-body" data-group="slide">
									<img src="<?php echo base_url();?>uploads/banner/<?php echo $row['attachment']?>" style="width:1140px;">
								  </div>
								</li>
							<?php }?>
						</ul>
					</div>
					<a class="slider-control left" href="#" data-jump="prev"><</a>
					<a class="slider-control right" href="#" data-jump="next">></a>
					
			   </div>
			  
				<script src="<?php echo base_url();?>design/public/js/responsive-slider.js"></script>
			<script src="<?php echo base_url();?>design/public/js/jquery.event.move.js"></script>
			 
			  <!--==============================================end of responsive slider============================-->   
				
					<?php }?>
                    
					             
    		
		<!-- start of the content ------------------------------------------>
        <div id="content">
          <div class="row">
            <div class="col-lg-9">            
            <?php
				if(!isset($view_file)){
					$view_file="";
				}
				if(!isset($module)){
					$module = $this->uri->segment(1);
				}
				if(($view_file!='') && ($module!='')){
					$path = $module."/".$view_file;
					$this->load->view($path);
				} 
				else {
					$new_description = str_replace("../../../","./",$description); //replacing image location as root location to display image
					echo nl2br($new_description);//put your designs here
				}
				
				//echo '<pre>';
				//print_r($this->session);
				//die();
            ?>
            </div>
            
            <div class="col-lg-3">
              <div class="support">
                <h4> Web Services</h4>
                <div class="row">
                  <div class="col-lg-12" style="text-align:justify;"> <img src="<?php echo base_url();?>design/frontend/img/fac1.jpg" width="90px" style="float:left; padding:10px;"> With latest trends in Web Services, we provide fast turnaround time with best quality at reasonable price. Static websites, database Driven, WordPress, Joomla, Drupal, CMS, Custom Programming etc.....  </div>
                  <a href="#" style="float:right; color:#06F; margin-right:12px;">Read More...</a> </div>
              </div>
              <div class="support">
                <div class="gallery">
                  <h4> Gallery</h4>
                  <div class="row">
                    <img src="<?php echo base_url();?>design/frontend/img/fac1.jpg" width="32%">
                    <img src="<?php echo base_url();?>design/frontend/img/fac2.jpg" width="32%">
                    <img src="<?php echo base_url();?>design/frontend/img/fac3.jpg" width="32%">
                  </div>
                  <div class="row">
                    <img src="<?php echo base_url();?>design/frontend/img/fac4.jpg" width="32%">
                    <img src="<?php echo base_url();?>design/frontend/img/fac5.jpg" width="32%">
                    <img src="<?php echo base_url();?>design/frontend/img/fac6.jpg" width="32%">
                  </div>
                </div>
              </div>
              <div class="support">
                <h4>Important Links</h4>
                <div class="link">
                <ul class="list-group"><li class="list-group-item"> <a href="#">Seminar</a></li>
                  <li class="list-group-item"><a href="#">Conference</a></li>
                  <li class="list-group-item"><a href="#">Portfolio</a></li>
                 </ul>
                </div>
              </div>
            </div>
            
          </div>
          <div class="footer">
            <div class="row">
              <div class="col-lg-8">
                  <div class="footernav">
                    <ul class="nav">
                    	<?php foreach($footernav as $footer){//still need to work on slug?> 
                        	<li>
                            	<?php if($footer['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                <a href="<?php echo $prefix.$footer['href'];?>" target="<?php echo $footer['target'];?>"><?php echo $footer['title']; ?></a>
                            </li>
                      	<?php } ?>
                    </ul>
                  </div>
              </div>
              <div class="col-lg-4">
                <p>Developed By <a href="http://upvedatech.com"><img src="<?php echo base_url();?>design/frontend/img/logo.png" width="150" height="70"></a></p>
              </div>
            </div>
            <p>All rights reserved! &copy; <?php echo date("Y"); ?></p>
          </div>
        </div><!---end-content--->
        
        
	</div><!--end of container-->
		<script src="<?php echo base_url();?>design/frontend/js/plugins.js"></script> 
        <script src="<?php echo base_url();?>design/frontend/js/main.js"></script> 
        <script src="<?php echo base_url();?>design/frontend/js/jquery.event.move.js"></script>
        <script src="<?php echo base_url();?>design/frontend/js/responsive-slider.js"></script>
        
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. --> 
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
    
    
</body>
</html>