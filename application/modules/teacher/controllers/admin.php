<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		//$this->load->module('admin_login/admin_login');
		//$this->admin_login->check_session_and_permission('teacher'); //module name is groups here	
	}
	
	function index()
	{	
           
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
                $this->load->view("admin/table",$data);
                
                //above process can be done on this way also
                //$data['view_file']="admin/table";
               // echo Modules::run('template/admin_template',$data);
	}
	
//	function get_data_from_post()
//	{
//		$data['name'] = $this->input->post('name', TRUE);		
//		
//			$update_id = $this->input->post('update_id', TRUE);
//		return $data;
//	}
//	
        
        function get_data_from_post()
	{
		$data['firstname'] = $this->input->post('firstname', TRUE);
                $data['lastname'] = $this->input->post('lastname', TRUE);
                $data['address'] = $this->input->post('address', TRUE);
                $data['phone_no'] = $this->input->post('phone_no', TRUE);
                $data['email'] = $this->input->post('email', TRUE);
                $data['status'] = $this->input->post('status', TRUE);
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['firstname'] = $row->firstname;
                        $data['lastname'] = $row->lastname;
                        $data['address'] = $row->address;
                        $data['phone_no'] = $row->phone_no;
                        $data['email'] = $row->email;
                        $data['status'] = $row->status;	

		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
//	function get_comments_from_db($update_id)
//	{
//		$query = $this->get_where($update_id);
//		foreach($teacher->result() as $row)
//		{			
//			$data['comments'] = $row->comments;				
//		}
//			return $data;
//	}
	
	function create()
	{
            
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
	
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
		
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
			$data['view_file'] = "admin/form";
			 $this->load->view("admin/form",$data);	
	}
	
         function submit()
        {          
            $data = $this->get_data_from_post();
            
            $update_id = $this->input->post('update_id', TRUE);
            if(is_numeric($update_id)){				
                    $this->_update($update_id, $data);
            } else {
            
                    $this->_insert($data);
                   
            }
            redirect('admin/teacher');
        }
		

	function get($order_by){
	$this->load->model('mdl_teacher');
	$query = $this->mdl_teacher->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_teacher');
	$query = $this->mdl_teacher->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_teacher');
	$this->mdl_teacher->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_teacher');
	$this->mdl_teacher->_update($id, $data);
	}
          function get_groups()
	{
	$this->load->model('course/mdl_course');
	$query = $this->mdl_course->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
        function delete()
	{	$this->load->model('mdl_teacher');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/teacher');
			}
		else
		{
			$this->mdl_teacher->_delete($delete_id);
			redirect('admin/teacher');
		}			
			
	}
}