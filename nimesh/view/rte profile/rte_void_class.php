<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Void-RTE-Attendance de' Islington</title>
  <link rel="shortcut icon" href="../../img/titleLogo.png">
  <link href="../../css/bootstrap/bootstrap.min.css" rel="stylesheet">
  <link href="../../css/rte profile/rte_view_attendance_bootstrap.css" rel="stylesheet">
</head>
<body id="mainBody">
  <div class="container-fluid">
	<div class="row">
	  <div id="sideSection" class="col-sm-12 col-sx-12 col-md-3 col-lg-3">
		<div class="panel panel-default">
		  <div class="panel-body">
			<div class="row">
			  <div id="profilePic" class="col-md-8 col-md-offset-2">
				<figure id="tm_pp" class="thumbnail logo-thumbnail">
				  <img src="../../img/profilePicture.jpg"/> 
				</figure>
			  </div>
			</div>
			<div class="row text-center blue-text">
			  <span class="glyphicon glyphicon-edit" aria-hidden="true"> Edit Picture</span>
			</div>
			<div class="row text-center blue-text">
			  <button class="btn btn-default">Chose file</button>
			</div>
			<ul class="list-group">
			  <li class="navSidebar list-group-item">
				<a href="#">My Dashboard</a>
			  </li>
			  <li class="navSidebar list-group-item">
				<a style="color:#E70F12;">View Attendance</a>
			  </li>
			  <li class="navSidebar list-group-item">
				<a href="#">Void Class</a>
			  </li>
			  <li class="navSidebar list-group-item">
				<a href="#">View Reports</a>
			  </li>
			  <li class="navSidebar list-group-item">
				<a href="#">Add Teacher</a>
			  </li>
			  <li class="navSidebar list-group-item">
				<a href="#">Edit Teacher Profile</a>
			  </li>
			  <li class="navSidebar list-group-item">
				<a href="#">Manage Schedule</a>
			  </li>
			  <li class="navSidebar list-group-item">
				<a href="#">Manage Groups</a>
			  </li>
			  <li class="navSidebar list-group-item">
				<a href="#">Import Students</a>
			  </li>
			  <li class="navSidebar list-group-item">
				<a href="#">Edit My Profile</a>
			  </li>
			  <li class="navSidebar list-group-item">
				<a href="#">Log Out</a>
			  </li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="col-md-9 col-lg-9">
		<div id="header" class="row">
		  <div id="headings" class="col-md-9 text-center">
			<h1 class="blue-text">ATTENDANCE DE' ISLINGTON</h1>
			<h3 class="red-text">Reports - RTE</h3>
		  </div>
		  <div id="logoContainer" class="col-md-3">
			<figure id="appLogo" class="thumbnail logo-thumbnail">
			  <img src="../../img/logo.png"/>
			</figure>
		  </div>
		</div>
		<ol class="breadcrumb">
		  <li><a href="#">Home</a></li>
		  <li><a href="#">Library</a></li>
		  <li class="active">Data</li>
		</ol>
		<div class="row">
		  <div id="middleSection">
		   <div id="dismissClassAttendance" class="col-md-8"> 
			<div class="panel panel-default site-panel">
			  <div class="panel-body">
				<h3 class="text-center"> 
				 Dismiss Class Attendance
			   </h3>
			   <form class="form-horizontal col-md-offset-2">
			   	<div class="form-group">
			   		<label class="col-md-4 control-label blue-text">Select Year - </label>
			   		<div class="col-md-6">
			   			<select class="form-control" id="voidClassSelectYear" name="voidClassSelectYear" >
			   				<option  value= "year1">&nbsp;Year 1</option> 
			   				<option  value= "year2">&nbsp;Year 2</option>
			   				<option  value= "year3">&nbsp;Year 3</option>
			   			</select>
			   		</div>
			   		<label class="col-md-4 control-label blue-text">Select Group - </label>
			   		<div class="col-md-6">
			   			<select class="form-control" id="voidClassSelectGroup" name="voidClassSelectGroup">
			   				<option  value= "L1C1">&nbsp;L1C1</option> 
			   				<option  value= "L1C2">&nbsp;L1C2</option>
			   				<option  value= "L1C3">&nbsp;L1C3</option>
			   				<option  value= "L1M1">&nbsp;L1M1</option>
			   				<option  value= "L1M2">&nbsp;L1M2</option>
			   				<option  value= "L1N1">&nbsp;L1N1</option>
			   				<option  value= "L1N2">&nbsp;L1N2</option>
			   				<option  value= "L2C1">&nbsp;L2C1</option>
			   				<option  value= "L2C2">&nbsp;L2C2</option>
			   				<option  value= "L2C3">&nbsp;L2C3</option>
			   				<option  value= "L2C4">&nbsp;L2C4</option>
			   				<option  value= "L2N1">&nbsp;L2N1</option>
			   				<option  value= "L2N2">&nbsp;L2N2</option>
			   				<option  value= "L2M1">&nbsp;L2M1</option>
			   				<option  value= "L3C1">&nbsp;L3C1</option>
			   				<option  value= "L3C2">&nbsp;L3C2</option>
			   				<option  value= "L3C3">&nbsp;L3C3</option>
			   				<option  value= "L3M1">&nbsp;L3M1</option>
			   				<option  value= "L3M2">&nbsp;L3M2</option>
			   				<option  value= "L3M3">&nbsp;L3M3</option>
			   				<option  value= "L3N1">&nbsp;L3N1</option>
			   				<option  value= "L3N2">&nbsp;L3N2</option>
			   			</select>
			   		</div>
			   		<label class="col-md-4 control-label blue-text">Select Date - </label>
			   		<div class="col-md-6">
			   			<select class="form-control" id="voidClassSelectClass" name="voidClassSelectClass">
			   				<option  value= "logicTutorial">&nbsp;Logic/Tutorial</option> 
			   				<option  value= "databaseLecture">&nbsp;Database/Lecture</option>
			   				<option  value= "pdfcLecture">&nbsp;PDFC/lecture</option>
			   				<option  value= "ebusinessLab">&nbsp;E-business/Lab</option>
			   			</select>
			   		</div>
			   	</div><!-- form-group -->
			   </form>
				
			<p class="col-md-offset-5">
			  <button class="btn btn-default" type="submit" name="btnViewAttendances">
				<img src="../../img/voidClassIconBtn.png">
				Dismiss class
			  </button>

			  <button class="btn btn-default" type="reset" name="cancel">
				<img src="../../img/cancelIcon.png">
				Cancel
			  </button>
			</p>
		  </div>
		</div>
	  </div>
	  <div id="QuickAccess" class="col-md-4 attendancesQuickAccess">
		<div class="panel panel-default site-panel">
		  <div class="panel-body">
			<h3 class="text-center">Quick Access</h3>
			<div class="row col-md-offset-3">
			  <button class="btn btn-default col-md-8" type="submit" name="addTeacher" onClick="#">
				<p style="width:2em; margin:auto auto;"><img src="../../img/addTeacherIcon.png"></p>
				<p style="width:90%; margin:auto auto; color:green">Add Teacher</p>
			  </button>

			  <button class="btn btn-default col-md-8" type="submit" name="voidClass" onClick="#">
				<p style="width:2em; margin:auto auto;"><img src="../../img/voidClassIcon.png"></p>
				<p style="width:90%; margin:auto auto; color:green">View Attendance</p>
			  </button>

			  <button class="btn btn-default col-md-8" type="submit" name="manageSchedule" onClick="#">
				<p style="width:2em; margin:auto auto;"><img src="../../img/manageScheduleIcon.png"></p>
				<p style="width:100%; margin:auto auto; color:green">Manage Schedule</p>
			  </button>

			  <button class="btn btn-default col-md-8" type="submit" name="viewReports" onClick="#">
				<p style="width:2em; margin:auto auto;"><img src="../../img/reportIcon.png"></p>
				<p style="width:90%; margin:auto auto; color:green">View Reports</p>
			  </button>
			</div>
		  </div>
		</div>
	  </div><!--QuickAccess-->
	</div>
  </div>
</div>
</div><!--row-->
</div><!--container-fluid -->
</body>
</html>