-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2016 at 05:45 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `islington`
--

-- --------------------------------------------------------

--
-- Table structure for table `ai_block`
--

CREATE TABLE IF NOT EXISTS `ai_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('live','draft') NOT NULL DEFAULT 'live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `ai_block`
--

INSERT INTO `ai_block` (`id`, `title`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Block1', 'block1', '2016-03-16', '2016-03-16', 'live'),
(11, 'Photos', 'photos', '2016-03-16', NULL, 'draft');

-- --------------------------------------------------------

--
-- Table structure for table `ai_class`
--

CREATE TABLE IF NOT EXISTS `ai_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `type` varchar(255) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `student_group_id` int(11) NOT NULL,
  `block_id` int(11) NOT NULL,
  `venu_id` int(11) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('live','draft') NOT NULL DEFAULT 'live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ai_class`
--

INSERT INTO `ai_class` (`id`, `date`, `start_time`, `end_time`, `type`, `subject_id`, `teacher_id`, `student_group_id`, `block_id`, `venu_id`, `ent_date`, `upd_date`, `status`) VALUES
(1, '2015-10-28', '2016-03-18 04:09:09', '1899-11-30 07:10:10', 'type', 1, 2, 3, 4, 5, '2016-03-17', '2016-03-17', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `ai_course`
--

CREATE TABLE IF NOT EXISTS `ai_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `status` enum('live','draft') NOT NULL DEFAULT 'live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ai_course`
--

INSERT INTO `ai_course` (`id`, `title`, `ent_date`, `upd_date`, `slug`, `status`) VALUES
(1, 'Photos', '2016-03-16', NULL, 'photos', 'draft'),
(2, 'ABCDs', '2016-03-16', NULL, 'abcds', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `ai_intake`
--

CREATE TABLE IF NOT EXISTS `ai_intake` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` int(11) NOT NULL,
  `upd_date` int(11) DEFAULT NULL,
  `status` enum('live','draft') NOT NULL DEFAULT 'live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ai_intake`
--

INSERT INTO `ai_intake` (`id`, `title`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Spring', 'spring', 2016, NULL, 'live'),
(2, 'Autumn', 'autumn', 2016, NULL, 'live');

-- --------------------------------------------------------

--
-- Table structure for table `ai_student`
--

CREATE TABLE IF NOT EXISTS `ai_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `intake_id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `course_id` int(11) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `status` enum('live','draft') NOT NULL DEFAULT 'live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ai_student_group`
--

CREATE TABLE IF NOT EXISTS `ai_student_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `status` enum('live','draft') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ai_student_group`
--

INSERT INTO `ai_student_group` (`id`, `title`, `ent_date`, `upd_date`, `slug`, `status`) VALUES
(1, 'group1', '2016-03-17', NULL, 'group1', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `ai_subject`
--

CREATE TABLE IF NOT EXISTS `ai_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `intake_id` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `status` enum('live','draft') NOT NULL DEFAULT 'live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ai_subject`
--

INSERT INTO `ai_subject` (`id`, `code`, `title`, `type`, `intake_id`, `year`, `ent_date`, `upd_date`, `slug`, `status`) VALUES
(2, '0', 'Photos', '0', 1, 0, '2016-03-17', NULL, 'photos', 'live'),
(3, '12x', 'computer', 'type', 1, 2015, '2016-03-17', '2016-03-17', 'computer', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `ai_teacher`
--

CREATE TABLE IF NOT EXISTS `ai_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone_no` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('live','draft') NOT NULL DEFAULT 'live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ai_teacher`
--

INSERT INTO `ai_teacher` (`id`, `firstname`, `lastname`, `address`, `phone_no`, `email`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'kjhh565', '56', '56', '565', '65', '2016-03-16', '2016-03-16', 'live'),
(2, 'harry', 'sijapati', 'street', '9856556554', 'me.harry@gmail.com', '2016-03-16', NULL, 'live'),
(3, 'harry', 'sijapati', 'street', '9856556554', 'me.harry@gmail.com', '2016-03-16', NULL, 'draft');

-- --------------------------------------------------------

--
-- Table structure for table `ai_venu`
--

CREATE TABLE IF NOT EXISTS `ai_venu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `block_id` int(11) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `status` enum('live','draft') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `ai_venu`
--

INSERT INTO `ai_venu` (`id`, `title`, `block_id`, `ent_date`, `upd_date`, `slug`, `status`) VALUES
(1, 'Venu', 0, '2016-03-16', '2016-03-16', 'venu', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('43776fed2c41f5e53184b1ddb6532edb', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', 1458182928, 'a:5:{s:9:"user_data";s:0:"";s:10:"permission";N;s:9:"logged_in";b:1;s:7:"user_id";s:1:"3";s:8:"group_id";s:1:"1";}'),
('48a638b098e2bbf33e36d921ef5279b6', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', 1458227002, 'a:4:{s:10:"permission";N;s:9:"logged_in";b:1;s:7:"user_id";s:1:"3";s:8:"group_id";s:1:"1";}'),
('4e1f313e4b42cc20468ebf1dc7e57d3c', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', 1458103283, 'a:5:{s:9:"user_data";s:0:"";s:9:"logged_in";b:1;s:7:"user_id";s:1:"3";s:8:"group_id";s:1:"1";s:10:"permission";N;}'),
('7a098d75cde5188e526fc9e9a0c16eee', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36', 1458009528, ''),
('8a4faabf67821bd897a602dae1f414c0', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', 1458106920, 'a:5:{s:9:"user_data";s:0:"";s:9:"logged_in";b:1;s:7:"user_id";s:1:"3";s:8:"group_id";s:1:"1";s:10:"permission";N;}'),
('a6df55d49dd746e5ec3a51fb56836ec4', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36', 1458009528, ''),
('a7e3ea77e1aa8d8b9664014203e15359', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36', 1458009528, ''),
('d65506899088e4cd7b45f900db6f8c46', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', 1458131332, ''),
('ef79a3cfcf39977a4d9fda67eb1b74d2', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.87 Safari/537.36', 1458133575, 'a:5:{s:9:"user_data";s:0:"";s:10:"permission";N;s:9:"logged_in";b:1;s:7:"user_id";s:1:"3";s:8:"group_id";s:1:"1";}'),
('fb7c689bbddc803bf35cc813461ab3d7', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36', 1458010964, 'a:4:{s:10:"permission";N;s:9:"logged_in";b:1;s:7:"user_id";s:1:"3";s:8:"group_id";s:1:"1";}');

-- --------------------------------------------------------

--
-- Table structure for table `up_banner`
--

CREATE TABLE IF NOT EXISTS `up_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `up_modules`
--

CREATE TABLE IF NOT EXISTS `up_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `up_modules`
--

INSERT INTO `up_modules` (`id`, `title`, `slug`, `ent_date`, `upd_date`) VALUES
(1, 'Banner', 'banner', '2014-02-28', NULL),
(2, 'Modules', 'modules', '2014-02-28', NULL),
(3, 'Navigation', 'navigation', '2014-02-28', NULL),
(4, 'Pages', 'pages', '2014-02-28', NULL),
(5, 'Groups', 'groups', '2014-02-28', NULL),
(6, 'Permissions', 'permissions', '2014-02-28', NULL),
(7, 'Settings', 'settings', '2014-02-28', NULL),
(8, 'Users', 'users', '2014-04-04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `up_navigation`
--

CREATE TABLE IF NOT EXISTS `up_navigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `navtype` varchar(255) NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `site_uri` varchar(255) DEFAULT NULL,
  `link_url` text,
  `position` varchar(255) DEFAULT NULL,
  `status` enum('draft','live') NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `up_navigation`
--

INSERT INTO `up_navigation` (`id`, `group_id`, `title`, `slug`, `parent_id`, `navtype`, `module_id`, `page_id`, `site_uri`, `link_url`, `position`, `status`) VALUES
(1, 1, 'Home', 'home', 0, 'Page', 0, 1, 'NULL', 'NULL', '1', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_navigation_group`
--

CREATE TABLE IF NOT EXISTS `up_navigation_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `up_navigation_group`
--

INSERT INTO `up_navigation_group` (`id`, `title`, `slug`) VALUES
(1, 'Header', 'header'),
(2, 'Footer', 'footer'),
(3, 'Sidebar', 'sidebar');

-- --------------------------------------------------------

--
-- Table structure for table `up_pages`
--

CREATE TABLE IF NOT EXISTS `up_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `up_pages`
--

INSERT INTO `up_pages` (`id`, `title`, `slug`, `description`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Home', 'home', '', '2014-02-28', NULL, 'live'),
(2, 'About Us', 'about-us', '<h3>About Us</h3>\n<p>\nUpveda Technology Pvt. Ltd was established in December 2010 as a software firm with an aim to provide software development services to variety of organizations including Government institutions and Small and Medium Sized Enterprises. Upveda Technology aims to foster customized software solutions to cater to the clients&#39; unique and real time needs. Upveda technology Pvt. Ltd emphasizes on creating a competitive ambience to its customers through its niche software solutions. Since its inception Upveda Technology Pvt. Ltd has believed in creating value to its customers through conceptualization consulting and software development services for real time problems.&nbsp;\n</p>\n', '2014-03-03', '2014-04-08', 'live'),
(3, 'Contact Us', 'contact-us', '<h3>Contact Us</h3>', '2014-04-07', '2014-04-08', 'live'),
(4, 'Under Construction', 'under-construction', '<p>\r\n<strong>Under Construction</strong>\r\n</p>\r\n<p>\r\nlkasjdfk jalskfj akjsf nalkjsfd halksjfd lakjsdf hlaksjd hfalkjsd hfalkjsd halksfj halksfd jhlaksjdf halkjsfd hlkjfdal ksjf lkajsfd k\r\n</p>\r\n<p>\r\nasdf lkasfdlk jaslkfdj aksjdf alkjsfdl ka&nbsp;\r\n</p>\r\n<p>\r\n&nbsp;\r\n</p>\r\n<ul>\r\n <li>sldfj lkajsfl kja</li>\r\n <li>safdlk jaslfkdj kaf</li>\r\n <li>asdfl; ajs;fid&nbsp;</li>\r\n <li>as;f a;if &nbsp;</li>\r\n</ul>\r\n<p>\r\n&nbsp;\r\n</p>\r\n', '2015-04-29', '2015-05-01', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_permissions`
--

CREATE TABLE IF NOT EXISTS `up_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `roles` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `up_permissions`
--

INSERT INTO `up_permissions` (`id`, `group_id`, `roles`) VALUES
(1, 2, 'a:2:{i:1;s:1:"1";i:3;s:1:"3";}');

-- --------------------------------------------------------

--
-- Table structure for table `up_settings`
--

CREATE TABLE IF NOT EXISTS `up_settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `meta_topic` varchar(255) NOT NULL,
  `meta_data` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `date_format` varchar(255) NOT NULL,
  `frontend_enabled` enum('open','closed') NOT NULL DEFAULT 'open',
  `unavailable_message` text NOT NULL,
  `favicon` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `up_settings`
--

INSERT INTO `up_settings` (`id`, `site_name`, `meta_topic`, `meta_data`, `contact_email`, `date_format`, `frontend_enabled`, `unavailable_message`, `favicon`, `logo`) VALUES
(1, 'Nepal Disaster Risk Reduction Portal', 'Nepal Disaster Risk Reduction Portal', 'Nepal Disaster Risk Reduction Portal', 'info@drrportal.gov.np', 'F j, Y', 'open', 'Sorry, this website is currently unavailable. Please try again later. Thank You!', 'favicon.png', 'logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `up_users`
--

CREATE TABLE IF NOT EXISTS `up_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `activation_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Registered User Information' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `up_users`
--

INSERT INTO `up_users` (`id`, `email`, `display_name`, `username`, `password`, `group_id`, `activation_code`, `created_on`, `last_login`) VALUES
(3, 'mukesh525@gmail.com', 'Moha Admin', 'admin', 'c2bc653ba87b56385b77c0fd8992545e09bf5b1985ae2d6e32be8de8c79bdd2a73dd16538f4a905aca9a5a61eb3eb70d3f62fc3a55284198d9de813273797e6b', 1, NULL, 2015, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `up_users_groups`
--

CREATE TABLE IF NOT EXISTS `up_users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `up_users_groups`
--

INSERT INTO `up_users_groups` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'user');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
